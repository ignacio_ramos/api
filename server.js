//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var movimientosJSON = require('./movimientosv2.json');

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send('Hola mundo nodejs');
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/clientes', function(req, res) {
  res.send('aqui estan los clientes devueltos nuevos');
});

app.get('/clientes/:idcliente', function(req, res) {
  res.send('aqui tiene al cliente ' + req.params.idcliente);
});

app.get('/movimientos/v1', function(req, res) {
  res.sendfile(path.join('movimientosv1.json'));
});

app.get('/movimientos/v2', function(req, res) {
  //res.sendfile(path.join('movimientosv1.json'));
  res.send(movimientosJSON);
});

app.get('/movimientos/v2/:index/:otro', function(req, res) {
  console.log(req.params.index);
  console.log(req.params.otro);

  res.send(movimientosJSON[req.params.index-1]);
});

app.get('/movimientos/v2', function(req, res) {
  console.log(req.query);

  res.send("recibido");
});

app.post('/', function(req, res) {
  res.send('Ricibido su peticion post');
});

app.post('/movimientos/v2', function(req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  res.send('movimiento dado de alta');
  //res.send('Ricibido su peticion post');
});
